package mail

import (
	"fmt"
	"net"
	"net/smtp"
)

//Variables Globales
var gaddress, gport, gidentity, guname, gpass, ghost string

type Cuerpo struct {
	Nombre   string
	Pedido   string
	Producto []string
}

//Inicializacion
func Init() {

	gaddress = "10.4.101.115"
	gport = "2025"
	gidentity = ""
	guname = "interface@grupocarsa.com"
	gpass = "vacaciones2013"
	ghost = "10.4.101.115"

}

func EnvioMail(dest string, subject string, body string) error {

	Init()

	headers := make(map[string]string)

	headers["from"] = "Musimundo <no_responder3@musimundo.com>"
	headers["to"] = dest
	headers["Subject"] = subject
	headers["MIME-version"] = "1.0"
	headers["Content-Type"] = "text/html; charset=UTF-8"
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	message += "\r\n" + body
	//fmt.Println(message)
	//tlsConf := &tls.Config{
	//	InsecureSkipVerify: true,
	//	ServerName:         ghost,
	//}

	serverport := ghost + ":" + gport
	conn, err := net.Dial("tcp", serverport)
	if err != nil {
		//fmt.Println("Dial")
		return err
	}
	client, err := smtp.NewClient(conn, ghost)
	if err != nil {
		//fmt.Println("NewClient ", err)
		return err
	}

	err = client.Mail("no_responder3@musimundo.com")
	if err != nil {
		//fmt.Println("Mail")
		return err
	}
	err = client.Rcpt(dest)
	if err != nil {
		//fmt.Println("RCPT")
		return err
	}
	w, err := client.Data()
	if err != nil {
		//fmt.Println("Data")
		return err
	}
	_, err = w.Write([]byte(message))
	if err != nil {
		//fmt.Println("Write")
		return err
	}
	err = w.Close()
	if err != nil {
		//fmt.Println("Close")
		return err
	}
	client.Quit()
	if err != nil {
		return err
	}

	return nil
}
