package main

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"
	s "strings"

	md "./data"
	mf "./function"
	mail "./mail"
	cti "bitbucket.org/carsateam/go-ctlritf"
	"bitbucket.org/carsateam/go-logger"
	"github.com/joho/godotenv"
)

var ValLista []md.Listas
var gPathHyb, gUserFtp, gSASFtp string

//var idprc int
var ErrCW []md.LineasFile

func AppendErrorCW(prod string) {
	var linea md.LineasFile
	linea = md.LineasFile{Linea: prod}
	ErrCW = append(ErrCW, linea)
}

//Carga todos los valores de Lista
func CargarLista(dbx *sql.DB) {
	var id, valor, nombre string
	cab, err := mf.RecuperarLista(dbx)

	if err != nil {
		logger.EscribirLog("CargarLista - Error al Buscar Listas", err)
	}

	for cab.Next() {
		cab.Scan(&id)
		//fmt.Println(id)
		det, err := mf.RecuperarListaValores(id, dbx)
		if err != nil {
			logger.EscribirLog("CargarLista - Error al Buscar Valores de Lista ", err)
		}

		gValores := []md.Lista{}
		for det.Next() {
			det.Scan(&valor, &nombre)
			//fmt.Println(valor, " - ", nombre)
			valor := md.Lista{Valor: valor,
				Nombre: nombre}
			gValores = append(gValores, valor)
		}
		lista := md.Listas{Id: id,
			Valores: gValores}
		ValLista = append(ValLista, lista)
	}
}

// Busca el valor de la Lista
func getValorLista(id string, valor string) string {
	val := s.Split(valor, ",")
	valor = val[0]
	for i := 0; i < len(ValLista); i++ {
		if ValLista[i].Id == id {
			for ii := 0; ii < len(ValLista[i].Valores); ii++ {
				if ValLista[i].Valores[ii].Valor == valor {
					return ValLista[i].Valores[ii].Nombre
				}
			}
		}
	}
	return ""
}

//Recupera todos los valores de atributos de los articulos
func ObtenerAtributos(dbx *sql.DB, dbs *sql.DB) string {
	var codigo, atributo, valor, lista string
	var anterior, antNorm int
	var catwAnt string
	var valornull sql.NullString
	var fechaHasta string

	anterior = 0
	antNorm = 0
	catw := ""

	datos, fechaHasta, err := mf.RecuperarAtributos(mf.Man, dbx)
	if err != nil {
		logger.EscribirLog("ObtenerAtributos - Error al Buscar Atributos", err)
		return fechaHasta
	}

	for datos.Next() {
		lista = ""
		valor = ""

		datos.Scan(&codigo, &atributo, &valornull, &lista)
		if valornull.Valid {
			valor = valornull.String
		} else {
			valor = ""
		}
		//logger.EscribirLog("Procesando:", codigo, atributo)
		valor = s.TrimSpace(valor)
		//valor = mf.Utf8_decode(valor)

		if lista != "" {
			valor = getValorLista(lista, valor)
			//valor = mf.Utf8_decode(s.TrimSpace(valor))
		}
		valor = s.ToUpper(valor)
		//atributo = mf.Utf8_decode(atributo)
		atributo = s.ToUpper(s.TrimSpace(atributo))

		if valor != "" && valor != "SSDD" && valor != "0099" {
			//atributo = mf.Utf8_decode(atributo)
			//Si es un producto Verde Agrego Atributo "Disponible en"
			cod, err := strconv.Atoi(codigo)
			if err != nil {
				logger.EscribirLog("ObtenerAtributos - Error convertir codigo ", codigo, err)
			}
			//Agrego esto para excluir los productos de musica
			if (cod > 299999) && (cod < 400000) {
				continue
			}

			if antNorm != cod {
				catw = mf.GetCatWeb(codigo, dbx)
				if catw == "" {
					catwAnt = ""
					antNorm = cod
					AppendErrorCW(codigo)
					continue
				}
				if catw == "EXCL" {
					continue
				}
				catwAnt = catw
			} else {
				catw = catwAnt
				if catw == "" {
					antNorm = cod
					continue
				}
			}
			antNorm = cod

			if (cod > 399999) && (cod < 500000) {
				if anterior != cod {
					mf.GraboAtribVerde(codigo, catw, dbx, dbs)
				}
				anterior = cod
			}
			//Descripcion Larga --> Tabla  [dbo].[descripcion_empresa]
			if s.ToUpper(atributo) == "DESCRIPCION LARGA" {
				//mf.GrabaDescLarga(codigo, atributo, valor, dbs)
				mf.ControlDLarga(codigo, atributo, valor, dbs)
			} else {
				fmt.Println("ObtenerAtributos - Se procesa ptroducto ", codigo, " - atributo :", atributo)
				mf.ControlAtributos(codigo, atributo, valor, catw, dbx, dbs)
			}

			if err != nil {
				logger.EscribirLog("ObtenerAtributos - Error al Insertar Atributos ", codigo, atributo, valor, err)
			}
		}
	}
	return fechaHasta
}
func LoadParamFTP(ent string) {
	err := godotenv.Load()
	if err != nil {
		logger.EscribirLog("LoadParamFTP - Error cargando .env file")
	}
	if ent == "PRD" {
		gPathHyb = os.Getenv("path_hybris")
		gUserFtp = os.Getenv("UserFtp")
		gSASFtp = os.Getenv("sas")
	} else {
		gPathHyb = os.Getenv("path_hybris")
		gUserFtp = os.Getenv("UserFtpq")
		gSASFtp = os.Getenv("sasq")
	}

}
func main() {
	//idprc = 101
	mf.Prc = 101
	logger.Init()
	logger.EscribirLog("Parametros de ejecucion: ", os.Args)

	//Valido los Parametros
	switch len(os.Args) {
	case 2:
		{
			mf.Ent = os.Args[1]
			mf.Man = ""
		}
	case 3:
		{
			mf.Ent = os.Args[1]
			mf.Man = os.Args[2]
		}
	default:
		{
			mf.Ent = os.Args[1]
			mf.Man = os.Args[2]
		}
	}
	if mf.Ent == "PRD" {
		mf.BRumbo = "rumbo_prd"
	} else {
		mf.BRumbo = "rumbo_qas"
	}
	lorigen := "V" + mf.Ent
	if mf.Man == "0" {
		cti.SetTabla("interfaz_sap:itf_exec_h")
		err := cti.ActEstado(mf.Prc, "EJECUTANDO", mf.Ent)
		if err != nil {
			logger.EscribirLog("Metadata - Error en control ejecucion itf", err)
			os.Exit(3)
		}

		//mf.Ent = "PRD"
		logger.EscribirLog("Parametros levantados - Ent/Modo:", mf.Ent, mf.Man)
		if mf.Ent == "" {
			logger.EscribirLog("Metadata - Faltan Parametro Entorno (QAS o PRD)")
			//err = cti.ActEspera(idprc, 60000, mf.Ent)
			err = cti.ActEstado(mf.Prc, "CON ERROR", mf.Ent)
			logger.EscribirLog(err)
			os.Exit(3)
		}
		if mf.Man == "" {
			mf.Man = "0"
		}
	}
	DBX, err := mf.InitConnectSQL(lorigen)
	if err != nil {
		logger.EscribirLog("Metadata - Error de conexion a Informix", err)
		os.Exit(3)
	}
	defer DBX.Close()
	DBS, err := mf.InitConnectSQL(mf.Ent)
	if err != nil {
		logger.EscribirLog("Metadata - Error de conexion a SQL Server", err)
		os.Exit(3)
	}
	defer DBS.Close()
	logger.EscribirLog("Metadata - Cargando Lista...")
	CargarLista(DBX)

	logger.EscribirLog("Metadata - Cargando Atributos Permitidos...")
	mf.CargaAtrHab(DBX)

	logger.EscribirLog("Metadata - Procesado Atributos...")
	fh := ObtenerAtributos(DBX, DBS)
	logger.EscribirLog("Fecha corte - " + fh)
	logger.EscribirLog("Metadata - Genero archivo de Atributos...")
	fileFTP := mf.GeneroArchivo(DBS)

	if fileFTP != "" {
		mf.FileToUtf8(fileFTP)
		LoadParamFTP(mf.Ent)
		logger.EscribirLog("Metadata - Transfiero Archivo de Atributos... Fin del Proceso", fileFTP)
		mf.FtpFile(fileFTP, gUserFtp, gSASFtp, gPathHyb)
	} else {
		logger.EscribirLog("Metadata - No Hay modificaciones a informar... Fin del Proceso")
	}
	if len(ErrCW) > 0 {
		cuerpo := ""
		for i := 0; i < len(ErrCW); i++ {
			cuerpo = cuerpo + ErrCW[i].Linea + " - "
		}
		mail.EnvioMail("edgardo.silveria@gcmusimundo.com", "Interface Metadatos - Prod sin Cat.Web", cuerpo)
	}
	if mf.Man == "0" {
		cti.SetFinEjecucion(mf.Ent, fh, mf.Prc)
		cti.ActEstado(mf.Prc, "OK", mf.Ent)
	}
	mf.CloseConnectIfx(DBX)
	mf.CloseConnectSQL(DBS)
	logger.Kill()
}
