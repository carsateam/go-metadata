package function

import (
	"database/sql"
	"fmt"
	"os"
	"os/exec"
	"strings"
	s "strings"
	"time"
	t "time"

	f "io/ioutil"

	md "../data"

	"bitbucket.org/carsateam/go-connect/connect/ifxdb"
	"bitbucket.org/carsateam/go-connect/connect/sqldb"
	cti "bitbucket.org/carsateam/go-ctlritf"
	"bitbucket.org/carsateam/go-logger"
	"golang.org/x/text/encoding/charmap"
)

type Tl []md.ListaAtr

var Ent string
var Man string
var BRumbo string
var Prc int

var LisAtr Tl
var File []md.LineasFile

//Cargo Lista de atributos habilitados x clase
func CargaAtrHab(dbx *sql.DB) {
	var pclase, patrib string
	atr, err := RecuperaAtrPermitidos(dbx)
	if err != nil {
		logger.EscribirLog("CargaAtrHab - Error al generar Lista de Atributos Permitidos", err)
	}

	for atr.Next() {
		atr.Scan(&pclase, &patrib)
		//pclase = Utf8_decode(pclase)
		//patrib = Utf8_decode(patrib)
		valor := md.ListaAtr{Clase: pclase, Latrib: patrib}
		LisAtr = append(LisAtr, valor)
	}
}
func InitConnectSQL(ent string) (*sql.DB, error) {
	con, err := sqldb.CreateConnect(ent)
	return con, err
}
func CloseConnectSQL(dbs *sql.DB) {
	dbs.Close()
}
func InitConnectIfx(ent string) (*sql.DB, error) {
	con, err := ifxdb.CreateConnect(ent)
	return con, err
}
func CloseConnectIfx(dbx *sql.DB) {
	dbx.Close()
}

func RecuperarLista(dbx *sql.DB) (*sql.Rows, error) {
	rows, err := dbx.Query("select distinct id_lista from valores_lista where not id_lista is null")
	return rows, err
}

func RecuperarListaValores(plista string, dbx *sql.DB) (*sql.Rows, error) {
	rows, err := dbx.Query("select id_valor,nombre from valores_lista where id_lista = " + plista)
	return rows, err
}

func RecuperaAtrPermitidos(dbx *sql.DB) (*sql.Rows, error) {
	rows, err := dbx.Query("select rca_clas, rca_lis_atr from rel_clas_atrib")
	return rows, err
}
func RecuperarAtributos(manual string, dbx *sql.DB) (*sql.Rows, string, error) {
	var qry string
	var lfechasta string
	lahora := time.Now().Format("2006-01-02 15:04:05")
	lfechasta = fmt.Sprint(lahora)
	//Recupero la fecha de ultima ejecucion
	if manual == "0" {
		lfechad, err := cti.GetDatosEjecucion(Ent, Prc)
		if err != nil {
			logger.EscribirLog(err)
			return nil, lfechasta, err
		}
		lfechad = s.ReplaceAll(lfechad, "T", " ")
		lfechad = s.ReplaceAll(lfechad, "-03:00", ".000")
		qry = "SELECT vm.cod_producto,  m.lbl_campo, vm.valor, m.id_lista" +
			" FROM visor.dbo.valores_metadata vm " +
			" INNER JOIN visor.dbo.metadata m on (vm.id_metadata = m.id)" +
			" INNER JOIN " + BRumbo + ".dbo.producto pro on (vm.cod_PRODUCTO = pro.codigo)" +
			" INNER JOIN " + BRumbo + ".dbo.articulo art on (art.id_producto = pro.id_producto)" +
			" WHERE pro.fecha_baja is null AND art.nivel_discontinuidad in (1,0,8,9,5) " +
			" AND vm.fecha_modificacion > Convert(datetime,'" + lfechad + "',102)" +
			" AND ( vm.valor is not null and vm.valor <> 'SSDD' and vm.valor<> '' )" +
			" AND m.lbl_campo <> 'Usuario' AND m.lbl_campo <> 'Fecha modificacion'" +
			//" Order by vm.cod_producto" +
			" UNION " +
			"SELECT vm.cod_producto,  m.lbl_campo, vm.valor, m.id_lista" +
			" FROM visor.dbo.valores_metadata vm " +
			" INNER JOIN visor.dbo.metadata m on (vm.id_metadata = m.id)" +
			" INNER JOIN " + BRumbo + ".dbo.producto pro on (vm.cod_PRODUCTO = pro.codigo)" +
			" INNER JOIN " + BRumbo + ".dbo.articulo art on (art.id_producto = pro.id_producto)" +
			" WHERE m.fecha_modificacion > Convert(datetime,'" + lfechad + "',102)" +
			" AND pro.fecha_baja is null AND art.nivel_discontinuidad in (1,0,8,9,5) " +
			" AND ( vm.valor is not null and vm.valor <> 'SSDD' and vm.valor<> '' )" +
			" AND m.lbl_campo <> 'Usuario' AND m.lbl_campo <> 'Fecha modificacion'" +
			" Order by vm.cod_producto"

	} else {
		articulos := GetListaArticulos()
		qry = "SELECT vm.cod_producto,  m.lbl_campo, vm.valor, m.id_lista" +
			" FROM visor.dbo.valores_metadata vm " +
			" INNER JOIN visor.dbo.metadata m on (vm.id_metadata = m.id)" +
			" INNER JOIN " + BRumbo + ".dbo.producto pro on (vm.cod_PRODUCTO = pro.codigo)" +
			" INNER JOIN " + BRumbo + ".dbo.articulo art on (art.id_producto = pro.id_producto)" +
			" WHERE vm.cod_producto in" + articulos + " and pro.fecha_baja is null AND art.nivel_discontinuidad in (1,0,8,9,5) " +
			" AND ( vm.valor is not null and vm.valor <> 'SSDD' and vm.valor<> '' )" +
			" AND m.lbl_campo <> 'Usuario' AND m.lbl_campo <> 'Fecha modificacion'" +
			" Order by vm.cod_producto"

	}
	rows, err := dbx.Query(qry)
	logger.EscribirLog("Recuperando novedades: ", qry)
	return rows, lfechasta, err
}

func GetListaArticulos() string {
	var lista, producto string
	dbx, err := InitConnectIfx(Ent)
	if err != nil {
		return lista
	}
	query := "select cod_art from basurero:art_mcom"
	rows, err := dbx.Query(query)
	if err != nil {
		return lista
	}
	i := 0
	for rows.Next() {
		rows.Scan(&producto)
		if i == 0 {
			lista = "('" + producto + "'"
			i = 1
		} else {
			lista = lista + ",'" + producto + "'"
		}
	}
	if i > 0 {
		lista = lista + ")"
	}
	return lista
}

func GraboAtribVerde(pcodigo string, catw string, dbx *sql.DB, dbs2 *sql.DB) {
	var demora, demoraActual, entMcom string
	if Ent == "PRD" {
		entMcom = "MPRD"
	} else {
		entMcom = "MQAS"
	}

	dbs, err := sqldb.CreateConnect(entMcom)
	if err != nil {
		logger.EscribirLog("GraboAtribVerde - Error al conectarse al entorno sql: " + entMcom)
	}
	query := "select dias_demora from [dbo].ARTICULOS_VERDE" +
		" where producto = " + pcodigo +
		" and getdate() between fecha_desde and fecha_hasta"
	err = dbs.QueryRow(query).Scan(&demora)
	if err != nil {
		return
	}

	query = "SELECT valor FROM [ficha_producto_emp] WHERE [codigo] = " + pcodigo +
		" AND atributo = 'DISPONIBLE EN'"
	err = dbs2.QueryRow(query).Scan(&demoraActual)
	if err == nil {
		if demora != demoraActual {
			ActualizoAtr(pcodigo, "DISPONIBLE EN", demora, dbx, dbs2)
		}

	} else {
		InsertoAtr(pcodigo, "DISPONIBLE EN", demora, catw, dbx, dbs2)
	}
	dbs.Close()

}

func ControlAtributos(pcodigo string, patributo string, pvalor string, pcatw string, dbx *sql.DB, dbs *sql.DB) {
	//logger.Init()
	var valor string

	query := "SELECT valor FROM [ficha_producto_emp] WHERE [codigo] = " + pcodigo +
		" AND atributo = '" + patributo + "'"

	err := dbs.QueryRow(query).Scan(&valor)
	if err == nil {

		if valor == s.ToUpper(pvalor) { //Si no cambio no hago nada
			//logger.EscribirLog("El valor del atributo no cambio", pcodigo, patributo, valor, pvalor)

		} else { //Si cambio lo actualizo
			ActualizoAtr(pcodigo, patributo, pvalor, dbx, dbs)
		}
	} else { // Si no existe hay que insertar el atributo si corresponde
		InsertoAtr(pcodigo, patributo, pvalor, pcatw, dbx, dbs)
	}
	//dbs.Close()
	//return res, err
}

func ControlDLarga(pcodigo string, patributo string, pvalor string, dbs *sql.DB) {
	//logger.Init()
	var valor string

	query := "SELECT descripcion FROM [descripcion_empresa] WHERE [codigo] = " + pcodigo

	err := dbs.QueryRow(query).Scan(&valor)
	if err == nil {

		if valor == s.ToUpper(pvalor) { //Si no cambio no hago nada
			//logger.EscribirLog("El valor del atributo no cambio", pcodigo, patributo, valor, pvalor)

		} else { //Si cambio lo actualizo
			ActualizoDLarga(pcodigo, patributo, pvalor, dbs)
		}
	} else { // Si no existe hay que insertar el atributo si corresponde
		InsertoDLarga(pcodigo, patributo, pvalor, dbs)
	}
	//dbs.Close()
	//return res, err
}
func ActualizoAtr(pcodigo string, patributo string, pvalor string, dbx *sql.DB, dbs *sql.DB) {

	query := "UPDATE [ficha_producto_emp]" +
		" SET valor = '" + s.ToUpper(pvalor) + "'," +
		" fecha_actualizado = Convert(datetime,GETDATE(),102)," +
		" XE_estado = 0" +
		"WHERE codigo = " + pcodigo +
		"AND atributo = '" + s.ToUpper(patributo) + "'"
	_, err := dbs.Exec(query)
	if err != nil {
		logger.EscribirLog("ActualizoAtr - Error al actualizar atributo", pcodigo, patributo, pvalor, err)
	}
	InsertoNovPrd(pcodigo)
}

func ActualizoDLarga(pcodigo string, patributo string, pvalor string, dbs *sql.DB) {

	query := "UPDATE [descripcion_empresa]" +
		" SET descripcion = '" + s.ToUpper(pvalor) + "'" +
		" WHERE codigo = " + pcodigo
	_, err := dbs.Exec(query)
	if err != nil {
		logger.EscribirLog("ActualizoDLarga - Error al actualizar Descripcion Larga", pcodigo, patributo, pvalor, err)
	}
	InsertoNovPrd(pcodigo)
}

func InsertoNovPrd(cod string) {

	dbx, err := InitConnectIfx(Ent)
	if err != nil {
		logger.EscribirLog("Metadata - Error de conexion a Informix", err)
		os.Exit(3)
	}
	defer dbx.Close()

	lhoy := t.Now().Format("2006-01-02 15:04:05")
	lhoystr := fmt.Sprint(lhoy)
	query := "insert into buzon_04:articulos values (0,'U'," +
		cod + ",'" + lhoystr + "')"
	_, err = dbx.Exec(query)
	if err != nil {
		logger.EscribirLog("Error al generar novedad producto ", cod)
	}

}
func InsertoAtr(pcodigo string, patributo string, pvalor string, catw string, dbx *sql.DB, dbs *sql.DB) {
	var lclase string

	if IsAtrComun(s.ToUpper(patributo)) {
		lclase = "ATRIBUTOS COMUNES"
	} else {
		lclase = GetCatWebClas(pcodigo, catw, dbx)
	}
	if (lclase != "") && (lclase != "ATRIBUTOS COMUNES") {
		lclase = GetAtributoHabilitado(lclase, patributo)
	}
	if lclase != "" {
		query := "INSERT INTO [ficha_producto_emp]" +
			"([empresa],[codigo],[atributo],[codigo_movimiento],[valor],[categoria],[XE_estado],[fecha_generado]" +
			",[fecha_actualizado],[estado])" +
			" VALUES( 'C'," + pcodigo + ",'" + patributo + "','A','" + s.ToUpper(pvalor) + "','" + s.ToUpper(lclase) +
			"',0,convert(datetime,GETDATE(),102),convert(datetime,GETDATE(),102),'')"

		_, err := dbs.Exec(query)
		if err != nil {
			logger.EscribirLog("InsertoAtr - Error al grabar atributo", pcodigo, patributo, pvalor, err)
		}
		InsertoNovPrd(pcodigo)
	}

}
func InsertoDLarga(pcodigo string, patributo string, pvalor string, dbs *sql.DB) {
	query := "INSERT INTO [descripcion_empresa]" +
		"([empresa],[codigo],[descripcion],[XE_estado])" +
		" VALUES( 'C'," + pcodigo + ",'" + pvalor + "',0)"

	_, err := dbs.Exec(query)
	if err != nil {
		logger.EscribirLog("InsertoDLarga - Error al grabar Descripcion Larga", pcodigo, patributo, pvalor, err)
	}
	InsertoNovPrd(pcodigo)

}
func GeneroArchivo(dbs *sql.DB) (arch string) {
	var codigo, atributo, movimiento, valor, categoria string
	var sline md.LineasFile

	query := "SELECT codigo,atributo,codigo_movimiento,SUBSTRING(valor,1,100),categoria" +
		" FROM [ficha_producto_emp] WHERE XE_estado = 0 "
	rows, err := dbs.Query(query)
	var primera int
	primera = 1
	if err == nil {
		for rows.Next() {
			rows.Scan(&codigo, &atributo, &movimiento, &valor, &categoria)

			linea := "\"" + s.TrimSpace(codigo) + "\",\"" +
				s.TrimSpace(atributo) + "\",\"" +
				s.TrimSpace(categoria) + "\",\"" +
				s.TrimSpace(s.ReplaceAll(valor, "\"", "")) + "\""
			if primera == 0 {
				linea = "\r\n" + linea
			} else {
				primera = 0
			}
			//linea = Utf8_decode(linea)
			sline = md.LineasFile{Linea: linea}
			File = append(File, sline)

		}
	}
	var file string
	file = ""
	if len(File) > 0 {
		//Genero el archivo en la PC
		var sitem string
		fecha := t.Now().Format("20060102")

		fechaHoy := fecha[2:]
		item := 0
		for {
			item = item + 1
			sitem = fmt.Sprintf("%04d", item)
			file = "C:/POS/HYBRIS/clasificacion-carsa-" + s.TrimSpace(fechaHoy) + s.TrimSpace(sitem) + ".csv"
			contenido, err := f.ReadFile(file)
			if err != nil {
				for i := 0; i < len(File); i++ {
					contenido = append(contenido, []byte(File[i].Linea)...)
				}
				err = f.WriteFile(file, contenido, 0777)
				break
			}
		}

		if err == nil {
			query = "UPDATE [ficha_producto_emp] SET XE_estado = 1 WHERE XE_estado = 0"
			_, err = dbs.Exec(query)
			if err != nil {
				logger.EscribirLog("GeneroArchivo - Error al actualizar [ficha_producto_emp]", err)
			}
		}
	}
	return file

}

func FtpFile(f string, usr string, sas string, path string) {
	var fileSinPath string

	fileSinPath = f[14:]

	comando := []string{"copy", s.TrimSpace(f), "https://" + usr + ".blob.core.windows.net" + path +
		fileSinPath + sas, "--from-to=LocalBlob", "--put-md5", "--blob-type=Detect", "--follow-symlinks"}

	out, err := exec.Command("azcopy.exe", comando...).Output()
	salida_err := fmt.Sprintf("%s", out)
	if err != nil {

		logger.EscribirLog("FtpFile - Error al transferir archivo por FTP", err, salida_err)
	} else {
		logger.EscribirLog(salida_err)
	}

}
func FileToUtf8(file string) {
	comando := []string{s.TrimSpace(file)}
	out, err := exec.Command("Conv.bat", comando...).Output()
	salida_err := fmt.Sprintf("%s", out)
	if err != nil {
		logger.EscribirLog("Error al convertir el archivo a UTF8", err, salida_err)
	}
}
func GetAtributoHabilitado(clas string, atrib string) (clase string) {

	for i := 0; i < len(LisAtr); i++ {
		if LisAtr[i].Clase == clas {
			latrib := s.ReplaceAll(LisAtr[i].Latrib, " ", "_")
			atrib = s.ToUpper(atrib)
			atrib = s.ReplaceAll(atrib, " ", "_")
			h := s.Split(latrib, ",")
			enc := false
			for i := 0; i < len(h); i++ {

				if strings.EqualFold(h[i], atrib) {
					enc = true
				}
			}
			//enc := s.Contains(latrib, atrib)
			if enc {
				return clas
			} else {
				return ""
			}
		}
	}
	return ""
}

func IsAtrComun(atributo string) bool {
	switch atributo {
	case
		"COLOR", "ALTO", "ANCHO", "PROFUNDIDAD", "PESO", "ORIGEN",
		"GARANTIA", "MODELO", "PDF", "GARANTÍA", "GARANTìA", "GARANTíA", "DISPONIBLE EN",
		"FECHA CERTIFICACION CARSA", "NRO CERTIFICACION CARSA", "ORGANISMO DE CERTIFICACION CARSA":
		return true
	}
	return false
}
func GetCatWeb(prod string, dbx *sql.DB) (cat string) {
	var catw string
	var rubro string
	query := "SELECT r.rubro_1 from " + BRumbo + ".dbo.producto p inner join " + BRumbo + ".dbo.rubro r on (p.id_rubro = r.id_rubro)" +
		" where p.codigo =  '" + prod + "'"
	err := dbx.QueryRow(query).Scan(&rubro)
	if err != nil {
		logger.EscribirLog("Error al determinar rubro producto", prod)
		return catw
	}
	if rubro == "PEL" || rubro == "MUS" {
		return "EXCL"
	}
	catw = ""
	query = "SELECT caw_cat_id FROM " + BRumbo + ".dbo.producto p" +
		" inner join " + BRumbo + ".dbo.rubro r on (p.id_rubro = r.id_rubro)" +
		" inner join visor.dbo.relacion_arbol_hyb ra on ( (ra.raw_rub_cod_uno = r.rubro_1 and ra.raw_tip_cnf = 'R')" +
		" OR ( ra.raw_rub_cod_uno = r.rubro_1 and ra.raw_rub_cod_dos = r.rubro_2 and ra.raw_tip_cnf = 'S')" +
		" OR ( ra.raw_rub_cod_uno = r.rubro_1 and ra.raw_rub_cod_dos = r.rubro_2 and " +
		"	  ra.raw_rub_cod_tre = r.rubro_3 and ra.raw_tip_cnf = 'T'))" +
		" inner join visor.dbo.categorias_hyb cat on (cat.caw_id = ra.raw_caw_id)" +
		" WHERE p.codigo = " + prod + " order by ra.raw_tip_cnf desc"

	err = dbx.QueryRow(query).Scan(&catw)
	if err != nil {
		logger.EscribirLog("GetCatWeb - No se pudo determinar la Cat Id del producto", prod)

	}
	return catw
}

func GetCatWebClas(prod string, catw string, dbx *sql.DB) (class string) {

	var clas string

	clas = ""
	query := "SELECT ccl_clas FROM cat_clasific WHERE ccl_cat_id = " + catw
	err := dbx.QueryRow(query).Scan(&clas)
	if err != nil {
		logger.EscribirLog("GetCatWebClas - No se pudo determinar la Cat Id del producto - Cat Clasific", prod)
	}
	return clas
}

func Utf8_decode(str string) string {
	var result string
	for i := range str {
		result += string(str[i])
	}
	return result
}

func ToUtf8New(str []byte) []byte {
	b := str
	//defino el decoder
	dec := charmap.Windows1256.NewDecoder()
	bUTF := make([]byte, len(b)*3)
	n, _, err := dec.Transform(bUTF, b, false)
	if err != nil {
		panic(err)
	}
	bUTF = bUTF[:n]
	return bUTF
}
